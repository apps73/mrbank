package com.bank.mrbank;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView ivBalance;
    private ImageView ivInvoices;
    private ImageView ivTransfer;
    private ImageView ivSavings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ivBalance = findViewById(R.id.ivBalance);
        ivInvoices = findViewById(R.id.ivInvoices);
        ivTransfer = findViewById(R.id.ivTransfer);
        ivSavings = findViewById(R.id.ivSavings);
        ivBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToBalanceScreen();
            }
        });
        ivInvoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToInvoiceScreen();
            }
        });
        ivTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToTransferScreen();
            }
        });
        ivSavings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToSavingScreen();
            }
        });
    }

    private void switchToBalanceScreen(){
        Intent intent = new Intent(MainActivity.this, Balance.class);
        startActivity(intent);
    }

    private void switchToInvoiceScreen(){
        Intent intent = new Intent(MainActivity.this, Invoices.class);
        startActivity(intent);
    }

    private void switchToTransferScreen(){
        Intent intent = new Intent(MainActivity.this, Transfer.class);
        startActivity(intent);
    }

    private void switchToSavingScreen(){
        Intent intent = new Intent(MainActivity.this, Savings.class);
        startActivity(intent);
    }

}
